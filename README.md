# Autenticação secundária - Brasil Participativo

Repositório para documentação da avaliação das tecnologias de autenticação e registro de usuários do ecossistema Decidim e necessidades de integração para atender às demandas da plataforma Brasil Participativo.


## Configuração

Execute `make docs` para acessar o servidor. Para construir os estáticos, execute `make build-docs`.

